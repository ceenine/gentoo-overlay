# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="An environment switcher for the shell"
HOMEPAGE="https://direnv.com"
SRC_URI="https://github.com/direnv/${PN}/archive/v${PV}.tar.gz"
SRC_URI+=" https://gitlab.com/ceenine/gentoo-overlay/-/raw/master/app-shells/direnv/${PN}-${PV}-deps.tar.xz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="test" # fails

DOCS=( {CHANGELOG,README}.md )

src_install() {
	einstalldocs
	emake DESTDIR="${D}" PREFIX="/usr" install
}
